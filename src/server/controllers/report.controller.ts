import { TRPCError } from "@trpc/server";
import { ReCaptchaSchema } from "../schemas/reCaptcha.schema";
import { MutationBody } from "../schemas/report.schema";

const baseUrl = process.env.REPORT_API_URL;
const reCaptchaVerifyUrl = "https://www.google.com/recaptcha/api/siteverify";
const reCaptchaSecret = process.env.RECAPTCHA_SECRET;

export const createReportHandler = async ({
  reportBody,
}: {
  reportBody: MutationBody;
}) => {
  const reCaptchaResponse = await fetch(
    `${reCaptchaVerifyUrl}?secret=${reCaptchaSecret}&response=${reportBody.reCaptchaToken}`,
    {
      method: "POST",
    }
  );

  const reCaptchaResult = ReCaptchaSchema.parse(await reCaptchaResponse.json());

  if (!reCaptchaResult.success)
    throw new TRPCError({ code: "PRECONDITION_FAILED" });

  const reportResponse = await fetch(`${baseUrl}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(reportBody),
  });

  if (reportResponse.status === 422)
    throw new TRPCError({ code: "UNPROCESSABLE_CONTENT" });
};

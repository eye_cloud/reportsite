import { z } from "zod";

export const ReCaptchaErrorCodeSchema = z.union([
  z.literal("missing-input-secret"),
  z.literal("invalid-input-secret"),
  z.literal("missing-input-response"),
  z.literal("invalid-input-response"),
  z.literal("bad-request"),
  z.literal("timeout-or-duplicate"),
]);

export const ReCaptchaSuccessSchema = z.object({
  success: z.literal(true),
  challenge_ts: z.string().datetime(),
  hostname: z.string(),
  score: z.number(),
});

export const ReCaptchaFailedSchema = z.object({
  success: z.literal(false),
  "error-codes": ReCaptchaErrorCodeSchema.array(),
});

export const ReCaptchaSchema = z.union([
  ReCaptchaSuccessSchema,
  ReCaptchaFailedSchema,
]);

import { z } from "zod";
import { DatabaseIdSchema, UuidSchema } from "./global.schema";

export const MutationBodySchema = z.object({
  machineId: UuidSchema,
  description: z.string(),
  reCaptchaToken: z.string(),
});
export type MutationBody = z.TypeOf<typeof MutationBodySchema>;

export const ParamsInputSchema = z.object({
  id: DatabaseIdSchema,
});
export type ParmasInput = z.TypeOf<typeof ParamsInputSchema>;

import { z } from "zod";

export const DatabaseIdSchema = z.number().int();
export const StringIdSchema = z.string();
export const UuidSchema = z.string(); // .uuid();

export const PersonSchema = z.object({
  id: z.string(),
  name: z.string(),
  email: z.string().email(),
  phone: z.string(),
  role: z.union([z.literal("Admin"), z.literal("Repairer")]).optional(),
});

export const PersonMetaSchema = PersonSchema.pick({ id: true });

export const RepairerSchema = PersonSchema.extend({
  role: z.literal("Repairer"),
});

export const AdminSchema = PersonSchema.extend({
  role: z.literal("Admin"),
});

import { initTRPC } from "@trpc/server";

const trpcServer = initTRPC.create();

export const { router, procedure, mergeRouters } = trpcServer;

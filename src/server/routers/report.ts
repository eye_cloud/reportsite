import { createReportHandler } from "../controllers/report.controller";
import { MutationBodySchema } from "../schemas/report.schema";
import { procedure, router } from "../trpc";

export const reportRouter = router({
  createReport: procedure
    .input(MutationBodySchema)
    .mutation(({ input }) => createReportHandler({ reportBody: input })),
});

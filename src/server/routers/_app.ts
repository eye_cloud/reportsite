import { router } from "../trpc";
import { reportRouter } from "./report";

export const appRouter = router({
  report: reportRouter,
});

export type AppRouter = typeof appRouter;

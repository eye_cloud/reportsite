import CenterBoxLayout from "@/components/CenterBoxLayout";
import { trpc } from "@/libs/trpc";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";
import { useForm } from "react-hook-form";

interface FormInput {
  machineId: string;
  description: string;
}

export default function ReportPage() {
  const createReportMutation = trpc.report.createReport.useMutation();

  const router = useRouter();
  const machineId = router.query.machineId;

  const { executeRecaptcha } = useGoogleReCaptcha();
  const [reCaptchaToken, setReCaptchaToken] = useState<string>("");
  const { register, handleSubmit } = useForm<FormInput>();

  const handleReCaptchaVerify = useCallback(async () => {
    if (!executeRecaptcha) return;

    setReCaptchaToken(await executeRecaptcha("submit"));
  }, [executeRecaptcha]);

  useEffect(() => {
    handleReCaptchaVerify();
  }, [handleReCaptchaVerify]);

  const onSubmit = handleSubmit((data) => {
    handleReCaptchaVerify();
    createReportMutation.mutate({ ...data, reCaptchaToken });
    router.push("/report/success");
  });

  if (machineId === undefined) return <></>;

  return (
    <>
      <CenterBoxLayout>
        <div className="flex flex-col gap-2">
          <h1 className="text-2xl font-semibold">Report Problem</h1>
          <div className="flex flex-col gap-1 rounded-md bg-slate-100 px-4 py-2">
            <h3 className="self-center font-bold text-slate-600">Device ID</h3>
            <p>{machineId}</p>
          </div>
          <form onSubmit={onSubmit}>
            <input
              type="hidden"
              defaultValue={machineId}
              {...register("machineId")}
            />
            <label htmlFor="description">Description</label>
            <textarea
              id="description"
              {...register("description", { required: true })}
              className="h-60 w-full resize-none rounded border border-zinc-200 px-2 py-1"
            />
            <button
              type="submit"
              className="g-recaptcha w-full rounded bg-blue-600 px-4 py-2 text-slate-50 hover:bg-blue-600/90 active:bg-blue-600"
            >
              Submit
            </button>
          </form>
        </div>
      </CenterBoxLayout>
    </>
  );
}

import CenterBoxLayout from "@/components/CenterBoxLayout";

export default function ReportSuccess() {
  return (
    <CenterBoxLayout>
      <h1 className="text-2xl font-bold">Report Submitted</h1>
      <p>Thank you for helping us improve the service</p>
    </CenterBoxLayout>
  );
}

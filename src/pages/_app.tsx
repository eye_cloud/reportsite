import { trpc } from "@/libs/trpc";
import "@/styles/globals.css";
import type { AppType } from "next/app";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";

const RECAPTCHA_KEY = process.env.NEXT_PUBLIC_RECAPTCHA_SITEKEY;

const App: AppType = ({ Component, pageProps }) => {
  if (RECAPTCHA_KEY === undefined)
    throw new Error(
      "ReCaptcha Initialization Fail: RECAPTCHA_KEY is undefined"
    );

  return (
    <>
      <GoogleReCaptchaProvider reCaptchaKey={RECAPTCHA_KEY}>
        <Component {...pageProps} />
      </GoogleReCaptchaProvider>
    </>
  );
};

export default trpc.withTRPC(App);

import { Head, Html, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      {/* <Script
        src="https://www.google.com/recaptcha/api.js"
        async
        defer
      ></Script> */}
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}

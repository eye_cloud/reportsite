import { beforeEach, describe, expect, it, vi } from "vitest";
import { postRequest } from "../reportAPI";

global.fetch = vi.fn();

describe("report API utils", () => {
  beforeEach(() => {
    process.env.REPORT_API_URL = "https://localhost:65565";

    vi.resetAllMocks();
  });

  it("it should construct a function", () => {
    expect(postRequest()).toBeTypeOf("function");
  });

  it("If env is not set", async () => {
    process.env.REPORT_API_URL = undefined;

    try {
      postRequest();
    } catch (e) {
      expect(e.message).toBe("No API URL provided");
    }
  });

  it("Fetch is called", async () => {
    await postRequest()({});

    expect(global.fetch).toHaveBeenCalledOnce();
  });
});

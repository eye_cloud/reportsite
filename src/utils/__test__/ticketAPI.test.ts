import { beforeEach, describe, expect, it, vi } from "vitest";
import { getRequest } from "../ticketAPI";

global.fetch = vi.fn();

describe("report API utils", () => {
  beforeEach(() => {
    process.env.TICKET_API_URL = "https://localhost:65565";

    vi.resetAllMocks();
  });

  it("it should construct a function", () => {
    expect(getRequest()).toBeTypeOf("function");
  });

  it("If env is not set", async () => {
    process.env.REPORT_API_URL = undefined;

    try {
      getRequest();
    } catch (e) {
      expect(e.message).toBe("No API URL provided");
    }
  });

  it("Fetch is called", async () => {
    await getRequest()("123");

    expect(global.fetch).toHaveBeenCalledOnce();
  });
});

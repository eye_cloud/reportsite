export const postRequest = () => {
  const url = process.env.REPORT_API_URL;
  if (!url) throw new Error("No API URL provided");

  return async (data: any) => {
    const res = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    return res;
  };
};

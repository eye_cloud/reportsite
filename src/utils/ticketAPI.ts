export const getRequest = () => {
  const url = process.env.TICKET_API_URL;
  if (!url) throw new Error("No API URL provided");

  return async (id: string) => {
    const res = await fetch(`${url}/${id}`);
    return res;
  };
};

import type { ReactNode } from "react";

export default function CenterBoxLayout({ children }: { children: ReactNode }) {
  return (
    <div className="mx-auto flex h-screen max-w-2xl items-center justify-center">
      <div className="h-fit rounded-lg border p-6 shadow-lg">{children}</div>
    </div>
  );
}

import FormSubmitButton from "./FormSubmitButton";
import FormTextArea from "./FormTextArea";

export default function ReportForm() {
  return (
    <form
      className="flex h-fit flex-col gap-4"
      method="post"
      action="/api/report"
    >
      <FormTextArea name="description" />
      <FormSubmitButton name="Submit" />
    </form>
  );
}

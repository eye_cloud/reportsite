export default function FormSubmitButton(props: { name: string }) {
  return (
    <button
      type="submit"
      className="inline-flex justify-center rounded-md border border-transparent bg-blue-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
    >
      {props.name}
    </button>
  );
}

export default function FormTextArea(props: { name: string }) {
  return (
    <div className="flex flex-col">
      <label htmlFor="name" className="text-sm font-medium text-gray-700">
        {props.name}
      </label>
      <textarea
        name={props.name}
        id={props.name}
        className="mt-1 block w-full resize-none rounded-md border  border-blue-400 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
      />
    </div>
  );
}

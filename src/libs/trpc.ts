import { AppRouter } from "@/server/routers/_app";
import { httpBatchLink } from "@trpc/client";
import { createTRPCNext } from "@trpc/next";

const getBaseUrl = () => {
  if (typeof window !== "undefined") return ""; // browser should use relative path;

  if (process.env.VERCEL_URL) return `https://${process.env.VERCEL_URL}`;

  if (process.env.INTERNAL_URL)
    return `http://${process.env.INTERNAL_URL}:${process.env.PORT ?? 3000}`;

  // localhost
  return `http://localhost:${process.env.PORT ?? 3000}`;
};

export const trpc = createTRPCNext<AppRouter>({
  config(opts) {
    return {
      links: [
        httpBatchLink({
          url: `${getBaseUrl()}/api/trpc`,

          headers: async () => ({}),
        }),
      ],
    };
  },

  ssr: false,
});
